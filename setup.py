from setuptools import setup

setup(
    name='Hackerboards-Utils',
    version='0.1',
    packages=['hackerboards_utils'],
    install_requires=[
        'pyyaml>=6',
        'tabulate',
        'humanize',
        'textual',
    ],
    include_package_data=True,
    package_data={'': ['*.tcss']},
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'hackerboards-bulk=hackerboards_utils.bulk:main',
            'hackerboards-edit=hackerboards_utils.editor:main',
            'hackerboards-check=hackerboards_utils.check:main',
        ]
    }
)
